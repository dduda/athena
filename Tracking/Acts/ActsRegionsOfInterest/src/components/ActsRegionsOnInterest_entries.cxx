/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/RegionsOfInterestCreatorAlg.h"
#include "src/FullScanRoICreatorTool.h"
#include "src/CaloBasedRoICreatorTool.h"
#include "src/TestRoICreatorTool.h"

DECLARE_COMPONENT( ActsTrk::RegionsOfInterestCreatorAlg )
DECLARE_COMPONENT( ActsTrk::FullScanRoICreatorTool )
DECLARE_COMPONENT( ActsTrk::CaloBasedRoICreatorTool )
DECLARE_COMPONENT( ActsTrk::TestRoICreatorTool )

